import React, { useState, Fragment, useEffect } from "react";

const HourComponent = ({ initHour, reset }) => {
  const [hour, setHour] = useState(null);
  const [isModify, setIsModify] = useState(false);

  useEffect(() => {
    setHour(initHour);
  }, [initHour, reset]);

  const handleChangehour = (hourModify) => {
    setHour(hourModify);
    setIsModify(false);
  };

  const hourLogic =
    hour != null ? (
      <Fragment>
        {!isModify ? (
          <div className="container_block">
            {hour} <span onClick={() => setIsModify(true)}>modifier</span>
          </div>
        ) : (
          <input
            type="time"
            onChange={(e) => handleChangehour(e.target.value)}
          />
        )}
      </Fragment>
    ) : null;
  return <div>{hourLogic}</div>;
};

export default HourComponent;
