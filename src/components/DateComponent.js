import React, { useState, Fragment, useEffect } from "react";

const DateComponent = ({ initDate, reset, monthOfYear }) => {
  const [date, setDate] = useState(null);
  const [isModify, setIsModify] = useState(false);

  useEffect(() => {
    setDate(initDate);
  }, [reset, initDate]);

  const handleChangeDay = (fullDate) => {
    const tabsDate = fullDate.split("-");
    console.log(tabsDate);
    const dayM = tabsDate[2];
    const year = tabsDate[0];
    const monthNumber = tabsDate[1];
    const monthLetter = monthOfYear.find(
      (elt) => elt.month === parseInt(monthNumber)
    );

    setDate(`${dayM} ${monthLetter.name} ${year}`);
    setIsModify(false);
  };

  const dateLogic =
    date != null ? (
      <Fragment>
        {!isModify ? (
          <div className="container_block">
            {date} <span onClick={() => setIsModify(true)}> modifier </span>
          </div>
        ) : (
          <input
            type="date"
            onChange={(e) => handleChangeDay(e.target.value)}
          />
        )}
      </Fragment>
    ) : null;
  return <div>{dateLogic}</div>;
};

export default DateComponent;
