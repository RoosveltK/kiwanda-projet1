import React, { useState, Fragment, useEffect } from "react";

const DayComponent = ({ initDay, reset, dayOfWeek }) => {
  const [day, setDay] = useState(null);
  const [isModify, setIsModify] = useState(false);

  useEffect(() => {
    setDay(initDay);
  }, [reset, initDay]);

  const handleChangeDay = (dayNumber) => {
    const dayChoose = dayOfWeek.find((elt) => elt.day === parseInt(dayNumber));
    setDay(dayChoose);
    setIsModify(false);
  };

  const dayLogic =
    day != null ? (
      <Fragment>
        {!isModify ? (
          <div class="container_block">
            {day.name} <span onClick={() => setIsModify(true)}> modifier </span>
          </div>
        ) : (
          <select onChange={(e) => handleChangeDay(e.target.value)}>
            <option>Sélectionnez le jour voulu</option>
            {dayOfWeek.map((days, index) => (
              <option key={index} value={days.day}>
                {days.name}
              </option>
            ))}
          </select>
        )}
      </Fragment>
    ) : null;

  return <div>{dayLogic}</div>;
};

export default DayComponent;
