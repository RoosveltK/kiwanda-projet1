import React, { Component } from "react";
import HourComponent from "./components/HourComponent";
import DayComponent from "./components/DayComponent";
import DateComponent from "./components/DateComponent";

const dayOfWeek = [
  {
    day: 1,
    name: "Lundi",
  },
  {
    day: 2,
    name: "Mardi",
  },
  {
    day: 3,
    name: "Mercredi",
  },
  {
    day: 4,
    name: "Jeudi",
  },
  {
    day: 5,
    name: "Vendredi",
  },
  {
    day: 6,
    name: "Samedi",
  },
  {
    day: 7,
    name: "Dimanche",
  },
];

const monthOfYear = [
  {
    month: 1,
    name: "Janvier",
  },
  {
    month: 2,
    name: "Février",
  },
  {
    month: 3,
    name: "Mars",
  },
  {
    month: 4,
    name: "Avril",
  },
  {
    month: 5,
    name: "Mai",
  },
  {
    month: 6,
    name: "Juin",
  },
  {
    month: 7,
    name: "Juillet",
  },
  {
    month: 8,
    name: "Aout",
  },
  {
    month: 9,
    name: "Septembre",
  },
  {
    month: 10,
    name: "Octobre",
  },
  {
    month: 11,
    name: "Novembre",
  },
  {
    month: 12,
    name: "Décembre",
  },
];

class App extends Component {
  state = {
    reset: false,
    initDay: null,
    initDate: null,
    initHour: `${new Date(new Date().getTime()).toLocaleTimeString("fr-FR", {
      hour12: false,
      hour: "numeric",
      minute: "numeric",
    })}`,
  };

  componentDidMount() {
    this.loadActualDay();
    this.interval = setInterval(() => this.hourChange(), 60000);
    this.loadActualDate();
  }

  handleReset = () => {
    this.loadActualDay();
    this.loadActualDate();
    this.setState({
      reset: !this.state.reset,
      initHour: `${new Date(new Date().getTime()).toLocaleTimeString("fr-FR", {
        hour12: false,
        hour: "numeric",
        minute: "numeric",
      })}`,
    });
  };

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  loadActualDay = () => {
    const actualDay = new Date().getDay();
    this.setState({ initDay: dayOfWeek.find((elt) => elt.day === actualDay) });
  };

  loadActualDate = () => {
    const dateM = new Date().getDate();
    const monthY = new Date().getMonth();
    const year = new Date().getFullYear();

    const monthLetter = monthOfYear.find((element) => element.month === monthY);
    this.setState({ initDate: `${dateM} ${monthLetter.name} ${year}` });
  };

  hourChange = () => {
    const hour = this.state.initHour.split(":");
    const stateMinut = parseInt(hour[1]);
    const hourH = parseInt(hour[0]);
    if (stateMinut === 59)
      this.setState({
        initHour: `${hourH < 9 ? `${"0" + hourH}` : `${hourH + 1}`}  : 00`,
      });
    else
      this.setState({
        initHour: `${hourH <= 9 ? `${"0" + hourH}` : `${hourH}`} : ${
          stateMinut < 9 ? `${"0" + stateMinut + 1}` : `${stateMinut + 1}`
        }`,
      });
  };

  render() {
    return (
      <div className="container">
        <div>
          <DayComponent
            reset={this.state.reset}
            dayOfWeek={dayOfWeek}
            initDay={this.state.initDay}
          />
          <HourComponent
            reset={this.state.reset}
            initHour={this.state.initHour}
          />
          <DateComponent
            initDate={this.state.initDate}
            monthOfYear={monthOfYear}
            reset={this.state.reset}
          />
        </div>
        <button className="btn" onClick={this.handleReset}>
          Réinitialiser
        </button>
      </div>
    );
  }
}
export default App;
